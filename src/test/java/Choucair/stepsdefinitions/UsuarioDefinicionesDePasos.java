package Choucair.stepsdefinitions;

import Choucair.steps.UsuarioChoucair;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class UsuarioDefinicionesDePasos {

    @Steps(shared = true)
    UsuarioChoucair usuario;

    @Given("^(.*) ingresa a la pagina Choucair$")
    public void ingresaAChoucair(String nombreDeUsuario){
        usuario.isCalled(nombreDeUsuario);
        usuario.ingresarAPagina();
    }

    @When("^(.*) selecciona la seccion Empleos$")
    public void ingresaASeccionEmpleos(String nombreDeUsuario){
        usuario.isCalled(nombreDeUsuario);
        usuario.seleccionarEmpleo();
    }
    @Then("^(.*) visualiza contenido y convocatorias$")
    public void validaSeccionEmpleos(String nombreDeUsuario) throws InterruptedException {
        usuario.isCalled(nombreDeUsuario);
        usuario.validarTexto();
    }

    @Given("^(.*) se encuentra en la sección Empleos$")
    public void validaSeccion(String nombreDeUsuario){
        usuario.isCalled(nombreDeUsuario);
        usuario.validarTexto();
    }

    @When("^(.) hace click en el botón (.)$")
    public void presionaBoton(String nombreDeUsuario, String boton){
        usuario.isCalled(nombreDeUsuario);
        usuario.presionarBotones();
    }
    @Then("^visualiza la pagina scrolleada en a posicion (.*)$")
    public void visualizaSeccion(Integer posicion) throws InterruptedException {
        usuario.validarBotones(posicion);
    }

    @Given("^(.*) selecciona un empleo disponible$")
    public void seleccionarEmpleoDisponible(String nombreDeUsuario) throws InterruptedException {
        usuario.isCalled(nombreDeUsuario);
        usuario.seleccionarOfertaLaboral();
    }
    @When("^selecciona la opcion para inscribirse al trabajo$")
    public void inscripcionAlTrabajo() throws InterruptedException {
        usuario.incribirAlTrabajo();
    }
    @And("^completa sus datos personales y labores$")
    public void completaDatosPersonales() throws InterruptedException {
        usuario.completarDatos();
    }

}