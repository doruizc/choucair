package Choucair.steps;

import Choucair.pageobject.PaginaPrincipal;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.steps.ScenarioActor;
import net.thucydides.core.annotations.Steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class UsuarioChoucair extends ScenarioActor {
    String actor;

    @Steps(shared =true)
    PaginaPrincipal paginaPrincipal;

    public void ingresarAPagina(){
        paginaPrincipal.setDefaultBaseUrl("https://www.choucairtesting.com/");
        paginaPrincipal.open();
    }

    public void seleccionarEmpleo(){
        paginaPrincipal.ir_a_seccion_empleos();
    }

    public void validarTexto(){
        String texto=paginaPrincipal.validar_seccion_empleos();
        assertThat(texto,equalToIgnoringCase("CONVOCATORIAS"));

    }

    public void presionarBotones() {
        paginaPrincipal.clickear_botones();
    }

    public void validarBotones(Integer posicion) throws InterruptedException {
        Integer scroll=(int)(long)(paginaPrincipal.validar_scroll());
        assertThat(scroll,equalTo(posicion));
    }

    public void seleccionarOfertaLaboral() throws InterruptedException {
        paginaPrincipal.seleccionar_empleo();
        String texto2=paginaPrincipal.validar_empleo_analista_pruebas();
        assertThat(texto2,equalTo("Analista de Pruebas Performance"));
    }

    public void incribirAlTrabajo() throws InterruptedException {
        paginaPrincipal.inscribirse();
    }

    public void completarDatos() throws InterruptedException {
        paginaPrincipal.completar_datos();
    }


}
