package Choucair.pageobject;

import jnr.ffi.annotations.In;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.JavascriptExecutor;

import java.awt.*;
import java.awt.event.KeyEvent;

public class PaginaPrincipal extends PageObject {

    Robot r =new Robot();

    static Integer count=0;

    @FindBy(xpath = "//*[@id=\"menu-item-550\"]/a")
    WebElementFacade btnEmpleo;

    @FindBy(xpath = "//*[@id=\"content\"]/div/div/div/div/div/section[7]/div/div/div/div/div/div[1]/div/h2")
    WebElementFacade textConvocatorias;

    @FindBy(xpath = "//*[@id=\"content\"]/div/div/div/div/div/section[2]/div/div/div[2]/div/div/div/div/div")
    WebElementFacade btnConvocatorias;

    @FindBy(xpath = "//*[@id=\"content\"]/div/div/div/div/div/section[2]/div/div/div[1]/div/div/div/div/div")
    WebElementFacade btnQueEsSerChoucair;

    @FindBy(xpath = "//*[@id=\"content\"]/div/div/div/div/div/section[2]/div/div/div[3]/div/div/div/div/div")
    WebElementFacade btnPrepararseParaAplicar;

    @FindBy(xpath = "//body/div[@id='page']/div[@id='content']/div/div/div/div/div/section/div/div/div/div/div/div/div/div/div/ul/li[1]")
    WebElementFacade btnAnalistaPruebas;

    @FindBy(xpath = "//body//div[@id='content']//div//div//div//div//div//div//div//div//div//div//div//div//div//div//li[4]")
    WebElementFacade moveElement;

    @FindBy(xpath = "//*[@id=\"post-387\"]/header/h1")
    WebElementFacade txtAnalistaPruebas;

    @FindBy(xpath = "//*[@id=\"post-387\"]/div/div[2]/div[3]/input")
    WebElementFacade btnInscribirse;

    @FindBy(xpath = "//input[@name='your-name']")
    WebElementFacade cajaNombre;

    @FindBy(xpath = "//input[@name='your-email']")
    WebElementFacade cajaCorreo;

    @FindBy(xpath = "//input[@name='tel']")
    WebElementFacade cajaCelular;

    @FindBy(xpath = "//input[@name='file-736']")
    WebElementFacade btnSeleccionarArchivo;



    public PaginaPrincipal() throws AWTException {
    }

    public void ir_a_seccion_empleos(){
        btnEmpleo.click();
    }

    public String validar_seccion_empleos(){
        //return textConvocatorias.waitUntilPresent().getText();
        return textConvocatorias.waitUntilEnabled().getText();

    }

    public void clickear_botones() {
        if(count==0){
            btnQueEsSerChoucair.click();
        }
        if(count==1){

            btnConvocatorias.click();
        }
        if(count==2){
            btnPrepararseParaAplicar.click();
        }
        count++;
    }

    public Long validar_scroll() throws InterruptedException {
        Thread.sleep(5000);
        Long defaultScroll= (Long) ((JavascriptExecutor) getDriver()).executeScript(" return window.scrollY");
        System.out.println("Default scroll height: "+defaultScroll);
        return defaultScroll;

    }

    public void seleccionar_empleo() throws InterruptedException {
        Actions actions= new Actions(getDriver());
        actions.moveToElement(moveElement).build().perform();
        btnAnalistaPruebas.waitUntilClickable().click();
    }

    public String validar_empleo_analista_pruebas(){
        return txtAnalistaPruebas.waitUntilEnabled().getText();
    }

    public void inscribirse() throws InterruptedException {
        Actions actions= new Actions(getDriver());
        actions.moveToElement(btnInscribirse).build().perform();
        r.keyPress(KeyEvent.VK_DOWN);
        r.keyRelease(KeyEvent.VK_DOWN);
        Thread.sleep(1000);
        r.keyPress(KeyEvent.VK_DOWN);
        r.keyRelease(KeyEvent.VK_DOWN);
        btnInscribirse.waitUntilEnabled().click();
    }

    public void completar_datos() throws InterruptedException {
        cajaNombre.sendKeys("Diego Oswaldo");
        cajaCorreo.sendKeys("oswaldo@gmail.com");
        cajaCelular.sendKeys("921888222");
        r.keyPress(KeyEvent.VK_TAB);
        r.keyRelease(KeyEvent.VK_TAB);
        Thread.sleep(300);
        r.keyPress(KeyEvent.VK_TAB);
        r.keyRelease(KeyEvent.VK_TAB);
        Thread.sleep(300);
        r.keyPress(KeyEvent.VK_TAB);
        r.keyRelease(KeyEvent.VK_TAB);
        Thread.sleep(300);
        r.keyPress(KeyEvent.VK_TAB);
        r.keyRelease(KeyEvent.VK_TAB);
        Thread.sleep(300);
        r.keyPress(KeyEvent.VK_TAB);
        r.keyRelease(KeyEvent.VK_TAB);
        Thread.sleep(300);
        r.keyPress(KeyEvent.VK_TAB);
        r.keyRelease(KeyEvent.VK_TAB);
        Thread.sleep(300);
        r.keyPress(KeyEvent.VK_TAB);
        r.keyRelease(KeyEvent.VK_TAB);
        Thread.sleep(300);
        r.keyPress(KeyEvent.VK_ENTER);
        r.keyRelease(KeyEvent.VK_ENTER);
        Thread.sleep(3000);
    }

}
