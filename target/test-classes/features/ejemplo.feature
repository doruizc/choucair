Feature: Automatizacion choucair
Scenario:  Ingresar a seccion Empleos

Given Usuario ingresa a la pagina Choucair
When Usuario selecciona la seccion Empleos
Then Usuario visualiza contenido y convocatorias

  Scenario Outline:Clickear botones de acción
    Given Usuario se encuentra en la sección Empleos
    When Usuario hace click en el botón <btn>
    Then visualiza la pagina scrolleada en a posicion <posicion>

    Examples:
      |btn                    |posicion|
      |Que es ser Choucair    |100     |
      |Convocatorias          |791     |
      |Prepararse para aplicar|200     |

  Scenario: Postularse al empleo
    Given User selecciona un empleo disponible
    When selecciona la opcion para inscribirse al trabajo
    And completa sus datos personales y labores